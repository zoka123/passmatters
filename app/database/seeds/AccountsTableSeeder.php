<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AccountsTableSeeder extends Seeder
{


    public function run()
    {

        $pc = new \PasswordCrypter();
        $faker = Faker::create();

        $user = \User::where(array('username' => 'dummy'))->first();
        $master = 'dummy';


        $account = new Account();
        $account->name = 'Facebook';
        $account->password = $pc->encryptPassword($master, 'facebook123');
        $account->user_id = $user->id;
        $account->save();


        $account = new Account();
        $account->name = 'Twitter';
        $account->password = $pc->encryptPassword($master, 'twitter123');
        $account->user_id = $user->id;
        $account->save();


        $account = new Account();
        $account->name = 'Gmail';
        $account->password = $pc->encryptPassword($master, 'gmail123');
        $account->user_id = $user->id;
        $account->save();


    }

}
<?php

class PasswordCrypterTest extends TestCase
{

    /**
     * Test PasswordCrypter class and its logic for encryption and decryption
     */
    public function testPasswordEncryption()
    {
        $faker = Faker\Factory::create();

        $pc = new PasswordCrypter();

        $password = $faker->word;
        $masterPassword = $faker->word;
        $encryptedString = $pc->encryptPassword($masterPassword, $password);
        $decryptedString = $pc->decryptPassword($encryptedString, $masterPassword);
        $this->assertEquals($decryptedString, $password);

        $password = sha1($faker->word);
        $masterPassword = sha1($faker->word);
        $encryptedString = $pc->encryptPassword($masterPassword, $password);
        $decryptedString = $pc->decryptPassword($encryptedString, $masterPassword);
        $this->assertEquals($decryptedString, $password);

        $password = $faker->sentence();
        $masterPassword = $faker->sentence();
        $encryptedString = $pc->encryptPassword($masterPassword, $password);
        $decryptedString = $pc->decryptPassword($encryptedString, $masterPassword);
        $this->assertEquals($decryptedString, $password);

        $password = $faker->realText(150);
        $masterPassword = $faker->realText(150);
        $encryptedString = $pc->encryptPassword($masterPassword, $password);
        $decryptedString = $pc->decryptPassword($encryptedString, $masterPassword);
        $this->assertEquals($decryptedString, $password);

    }


    public function testPasswordReEncryption()
    {
        $faker = Faker\Factory::create();
        $pc = new PasswordCrypter();

        $password = $faker->sentence();
        $masterPassword = $faker->sentence();
        $masterPassword2 = $faker->sentence();

        $encryptedString = $pc->encryptPassword($masterPassword, $password);
        $encryptedString2 = $pc->reCryptStringWithNewMaster($masterPassword, $masterPassword2, $encryptedString);

        $decryptedString = $pc->decryptPassword($encryptedString, $masterPassword);
        $decryptedString2 = $pc->decryptPassword($encryptedString2, $masterPassword2);

        $this->assertNotEquals($encryptedString,$encryptedString2);

        $this->assertEquals($decryptedString, $password);
        $this->assertEquals($decryptedString2, $password);
        $this->assertNotEquals($pc->decryptPassword($encryptedString, $masterPassword2), $password);
    }

}

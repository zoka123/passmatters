<?php

class ApiController extends Controller
{

    /**
     * Setup CSRF protection
     */
    public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => array('post', 'delete', 'put'), 'except' => array('login', 'logout')));
    }

    /**
     * Api endpoint for login
     * @return array
     */
    public function login()
    {
        $username = Input::get('username');
        $password = Input::get('password');

        if (empty($username) || empty($password)) {
            return array(
                'code'    => 0,
                'message' => 'Credentials not provided'
            );
        }

        if (Auth::validate(array('username' => $username, 'password' => $password))) {
            /** @var  \User $user */
            $user = User::where(array('username' => $username))->first();

            $token = new Token();
            $token->token = sha1(uniqid('user_', true));
            $token->user_id = $user->id;

            if ($token->save()) {
                return array(
                    'code'    => 1,
                    'message' => 'Success',
                    'token'   => $token->token
                );
            } else {
                return array(
                    'code'    => 0,
                    'message' => 'Error storing token ' . $user->errors()->first(),
                );
            }
        } else {
            return array(
                'code'    => 0,
                'message' => 'Bad credentials'
            );
        }
    }


    /**
     * Api endpoint for adding new account
     * @return array
     */
    public function addAccount()
    {
        $name = Input::get('name');
        $password = Input::get('password');
        $password_confirmation = Input::get('password_confirmation');
        $master_password = Input::get('master_password');

        if (empty($name) || empty($password) || empty($master_password)) {
            return array(
                'code'    => 0,
                'message' => 'Missing data',
            );
        }

        if ($password !== $password_confirmation) {
            return array(
                'code'    => 0,
                'message' => 'Password mismatch',
            );
        }

        if (Auth::validate(array('username' => Auth::getUser()->username, 'password' => $master_password))) {

            $pc = new PasswordCrypter();
            $password = $pc->encryptPassword($master_password, $password);
            unset($master_password);

            $account = new Account();
            $account->name = $name;
            $account->password = $password;
            $account->user_id = Auth::getUser()->id;
            $account->save();

            return array(
                'code'    => 1,
                'message' => 'Success',
            );
        } else {
            return array(
                'code'    => 0,
                'message' => 'Master password mismatch',
            );
        }
    }


    /**
     * Api endpoint for updating account
     * @param $id
     * @return array
     * @throws Exception
     */
    public function updateAccount($id)
    {
        $this->checkOwner($id);

        $name = Input::get('name');
        $password = Input::get('password');
        $password_confirmation = Input::get('password_confirmation');
        $master_password = Input::get('master_password');

        if (empty($name) || empty($password) || empty($master_password)) {
            return array(
                'code'    => 0,
                'message' => 'Missing data',
            );
        }

        if ($password !== $password_confirmation) {
            return array(
                'code'    => 0,
                'message' => 'Password mismatch',
            );
        }


        if (Auth::validate(array('username' => Auth::getUser()->username, 'password' => $master_password))) {

            $pc = new PasswordCrypter();
            $password = $pc->encryptPassword($master_password, $password);
            unset($master_password);

            $account = Account::findOrFail($id);
            $account->name = $name;
            $account->password = $password;
            $account->save();

            return array(
                'code'    => 1,
                'message' => 'Success',
            );
        } else {
            return array(
                'code'    => 0,
                'message' => 'Master password mismatch',
            );
        }
    }


    /**
     * Api endpoint for deleting account
     * @param $id
     * @return array
     * @throws Exception
     */
    public function deleteAccount($id)
    {
        $this->checkOwner($id);
        $master_password = Input::get('master_password');

        if (Auth::validate(array('username' => Auth::getUser()->username, 'password' => $master_password))) {

            if (Account::destroy($id)) {
                return array(
                    'code'    => 1,
                    'message' => 'Success',
                );
            } else {
                return array(
                    'code'    => 0,
                    'message' => 'Error deleting',
                );
            }

        } else {
            return array(
                'code'    => 0,
                'message' => 'Master password mismatch',
            );
        }
    }


    /**
     * Api endpoint for account listing
     * @return array
     */
    public function listAccounts()
    {

        $user = Auth::getUser();
        if (!empty($user)) {
            return array(
                'code'     => 1,
                'message'  => 'Success',
                'accounts' => $user->accounts()->select(array('id', 'name'))->get(),
            );
        } else {
            return array(
                'code'    => 0,
                'message' => 'Invalid token',
            );
        }
    }


    /**
     * Returns flag if current user is owner of an account
     * @param $account_id
     * @return bool
     * @throws Exception
     */
    protected function checkOwner($account_id)
    {
        $account = Account::findOrFail($account_id);
        if (!Auth::getUser()->isOwnerOfAccount($account)) {
            throw new Exception('You are not owner');
        } else {
            return true;
        }
    }


    /**
     * Returns details for given account
     * @param $id
     * @return string
     * @throws Exception
     */
    public function getAccount($id)
    {
        $this->checkOwner($id);

        return Account::findOrFail($id)->toJson();
    }


    /**
     * Api endpoint for logout
     * @return array
     * @throws Exception
     */
    public function logout()
    {
        $token = Input::get('token');
        $token = \Token::where(array('token' => $token))->firstOrFail();
        $token->delete();

        return array(
            'code'    => 1,
            'message' => 'User logged out',
        );

    }


    /**
     * Edit profile endpoint.
     * Application have to load all accounts and reEncrypt each password with new master key
     * @return array
     */
    public function editProfile()
    {
        $oldPass = Input::get('old_master_password');
        $newPassword = Input::get('new_master_password');
        $newPasswordConf = Input::get('new_master_password_confirmation');

        if (empty($oldPass) || empty($newPassword) || empty($newPasswordConf)) {
            return array(
                'code'    => 0,
                'message' => 'Missing data',
            );
        }

        if ($newPassword !== $newPasswordConf) {
            return array(
                'code'    => 0,
                'message' => 'Password mismatch',
            );
        }
        unset($newPasswordConf);


        if (Auth::validate(array('username' => Auth::getUser()->username, 'password' => $oldPass))) {

            $user = Auth::getUser();
            try {
                $pc = new PasswordCrypter();
                $accounts = Auth::getUser()->accounts()->get();
                foreach ($accounts as $account) {

                    /** @var Account $account */
                    $account->password = $pc->reCryptStringWithNewMaster($oldPass, $newPassword, $account->password);
                    $account->save();
                }
                $user->password = $newPassword;
                $user->updateUniques(User::$updateRulesWithoutPasswordConfirmation);

                return array(
                    'code'    => 1,
                    'message' => 'Success',
                );
            } catch (Exception $e) {
                Log::error('Error during reeencryption: ', array($e));

                return array(
                    'code'    => 0,
                    'message' => 'Error',
                );
            }
        } else {
            return array(
                'code'    => 0,
                'message' => 'Master password mismatch',
            );
        }
    }

}
<?php

class AppController extends BaseSiteController
{

    public function index()
    {
        $accounts = Auth::getUser()->accounts()->get();

        return $this->render('App.home', compact("accounts"));
    }

    public function editProfile()
    {
        $user = Auth::getUser();

        return $this->render('App.edit-profile', compact('user'));

    }

}
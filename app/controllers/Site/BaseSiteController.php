<?php

use Zantolov\Zamb\Controllers\BaseController;

class BaseSiteController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->setParam('menu', new \Zamb\Menu\PublicMenu());
    }

} 
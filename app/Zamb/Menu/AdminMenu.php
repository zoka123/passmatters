<?php

namespace Zamb\Menu;

use Zantolov\Zamb\Menu\Menu;
use Zantolov\Zamb\Menu\MenuItem;

class AdminMenu extends Menu
{
    public function __construct()
    {
        # Home item
        $this->addMenuItem(new MenuItem('Home', \URL::route('Admin.Dashboard')));

        # User management
        $users = new MenuItem('<i class="fa fa-users"></i> User Management');
        $users->addChildren(new MenuItem('<i class="fa fa-user"></i> Users', \URL::route('Admin.Users.Index')));
        $users->addChildren(new MenuItem('<i class="fa fa-cube"></i> Roles', \URL::route('Admin.Roles.Index')));
        $users->addChildren(new MenuItem('<i class="fa fa-lock"></i> Permissions', \URL::route('Admin.Permissions.Index')));
        $this->addMenuItem($users);

        #Static Pages
        $this->addMenuItem(new MenuItem('<i class="fa fa-file"></i> Static pages', \URL::route('Admin.StaticPages.Index')));

#        $multimedia = new MenuItem('<i class="fa fa-image"></i> Multimedia');
#        $multimedia->addChildren(new MenuItem('<i class="fa fa-image"></i> Images', \URL::route('Admin.Images.Index')));
#        $this->addMenuItem($multimedia);

        #Tags
        $this->addMenuItem(new MenuItem('<i class="fa fa-tag"></i> Tags', \URL::route('Admin.Tags.Index')));

        # Params
        $this->addParam('rightMenu', 'Navigation.profile-menu-item');


    }

} 
<?php

namespace Zamb\Menu;

use Zantolov\Zamb\Menu\MenuItem;

class PublicMenu
{
    const USER_PROFILE_ITEM = 'Navigation.profile-menu-item';

    protected $rightMenu = null;
    protected $menu = array();

    public function __construct()
    {
        if (\Auth::check()) {
            $this->rightMenu = self::USER_PROFILE_ITEM;
        } else {
            $this->menu[] = new MenuItem('Register', \URL::route('user.create'));
            $this->menu[] = new MenuItem('Login', \URL::route('user.login'));
        }
    }

    public function compose($view)
    {
        $view->with('menu', $this->getMenu());
    }

    public function getMenu()
    {
        return $this->menu;
    }

    public function getRightMenu()
    {
        return $this->rightMenu;
    }

}

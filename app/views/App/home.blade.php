@extends('Layouts.default')

@section('stylesheets')
    @parent
    <link rel="stylesheet" href="{{asset('assets/css/bootstrapValidator.min.css')}}">
@endsection

@section('content')
    @include('Navigation.menu')

    <div class="container">
        <div class="col-md-6">
            <h1>My accounts <span class="loader"><i class="fa fa-spin fa-refresh"></i></span></h1>

            <div>
                @foreach($accounts as $account)
                    <a href="javascript:void(0)" data-id="{{{ $account['id'] }}}"
                       class="btn btn-block btn-primary account-trigger">
                        <i class="fa fa-file fa-2x"></i> {{{ $account['name'] }}}
                    </a>
                @endforeach
            </div>
        </div>

        <div class="col-md-6">

            <h1>Add / Edit account
                <button onclick="app_controller.addNew()" class=" pull-right btn btn-sm btn-success"><i
                            class="fa fa-plus"></i> Add new
                </button>
            </h1>

            {{ Form::open(array('url' => URL::route('Api.addAccount'), 'id' => "accountForm", 'autocomplete' => "off")) }}
            <input type="hidden" name="account_id" value="0">

            <div class="error-container alert-danger alert" style="display: none"></div>

            <div class="form-group">
                <label for="name">Account name</label>
                <input name="name" placeholder="Account name" class="form-control"/>
            </div>
            <div class="form-group">
                <label for="name">Account password</label>
                <input type="password" name="password" placeholder="Account password" class="form-control"/>
            </div>
            <div class="form-group">
                <label for="name">Account password confirmation</label>
                <input type="password" name="password_confirmation" placeholder="Account password confirmation"
                       class="form-control"/>
            </div>
            <div class="form-group">
                <label for="name">Master password</label>
                <input type="password" name="master_password" placeholder="Master password" class="form-control"/>
            </div>
            <button class="btn btn-block btn-success" type="submit">Submit</button>
            <hr>
            <p class="red text-center">
                <a href="javascript:void(0);" class="deleteTrigger"><i class="fa fa-trash"></i> Delete</a>
            </p>
            {{ Form::close() }}


        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script src="{{asset('assets/js/bootstrapValidator.min.js')}}"></script>
    <script src="{{asset('assets/js/app.js')}}"></script>
    <script>
        $(function () {
            app_controller.base_url = '{{ Url::to('/') }}';
            app_controller.get_account_url = '{{ Url::route('Api.getAccount', '') }}/';
            app_controller.update_account_url = '{{ Url::route('Api.updateAccount', '') }}/';
            app_controller.delete_account_url = '{{ Url::route('Api.deleteAccount', '') }}/';

            app_controller.init();

            $('#accountForm').bootstrapValidator({
                message: 'This value is not valid',
                feedbackIcons: {
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                },
                fields: {
                    name: {
                        message: 'The name is not valid',
                        validators: {
                            notEmpty: {
                                message: 'The name is required and cannot be empty'
                            },
                            stringLength: {
                                min: 1,
                                max: 30,
                                message: 'The name must be more than 6 and less than 30 characters long'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9_ čćžšđČĆŽŠĐ-]+$/,
                                message: 'The name can only consist of alphabetical, number and underscore'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'The field is required and cannot be empty'
                            },
                            identical: {
                                field: 'password_confirmation',
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    },
                    password_confirmation: {
                        validators: {
                            notEmpty: {
                                message: 'The field is required and cannot be empty'
                            },
                            identical: {
                                field: 'password',
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    },
                    master_password: {
                        validators: {
                            notEmpty: {
                                message: 'The field is required and cannot be empty'
                            }
                        }
                    }

                }
            }).on('success.form.bv', function (e) {
                // Prevent form submission
                e.preventDefault();

                // Get the form instance
                var $form = $(e.target);

                // Get the BootstrapValidator instance
                var bv = $form.data('bootstrapValidator');

                app_controller.submitForm($form);
            });
            ;
        });
    </script>
@endsection
@extends('Layouts.default')

@section('stylesheets')
    @parent
    <link rel="stylesheet" href="{{asset('assets/css/bootstrapValidator.min.css')}}">
@endsection

@section('content')
    @include('Navigation.menu')

    <div class="container">
        <div class="col-md-6">
            <h2>User details</h2>

            <p>
                <i class="fa fa-user"></i> {{{ $user->username }}}
            </p>

            <p>
                <i class="fa fa-envelope"></i> {{{ $user->email }}}
            </p>

            <hr>
            <h2>Change your master password</h2>

            {{ Form::open(array('url' => URL::route('Api.EditProfile'), 'id' => "profileForm")) }}

            <div class="error-container alert-danger alert" style="display: none"></div>

            <div class="form-group">
                <label for="name">Old Master password</label>
                <input type="password" name="old_master_password" placeholder="Old Master password"
                       class="form-control"/>
            </div>

            <div class="form-group">
                <label for="name">New Master password</label>
                <input type="password" name="new_master_password" placeholder="Account password" class="form-control"/>
            </div>

            <div class="form-group">
                <label for="name">New Master password confirmation</label>
                <input type="password" name="new_master_password_confirmation"
                       placeholder="Account password confirmation"
                       class="form-control"/>
            </div>

            <button class="btn btn-block btn-success" type="submit">Submit</button>
            <hr>
            <p class="red text-center">
                <a href="javascript:void(0);" class="deleteTrigger"><i class="fa fa-trash"></i> Delete</a>
            </p>
            {{ Form::close() }}

        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script src="{{asset('assets/js/bootstrapValidator.min.js')}}"></script>
    <script>
        $(function () {
            $('#profileForm').bootstrapValidator({
                message: 'This value is not valid',
                feedbackIcons: {
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                },
                fields: {
                    old_master_password: {
                        validators: {
                            notEmpty: {
                                message: 'Cannot be empty'
                            }
                        }
                    },
                    new_master_password: {
                        validators: {
                            notEmpty: {
                                message: 'The field is required and cannot be empty'
                            },
                            identical: {
                                field: 'new_master_password_confirmation',
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    },
                    new_master_password_confirmation: {
                        validators: {
                            notEmpty: {
                                message: 'The field is required and cannot be empty'
                            },
                            identical: {
                                field: 'new_master_password',
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    }
                }
            }).on('success.form.bv', function (e) {
                // Prevent form submission
                e.preventDefault();

                // Get the form instance
                var $form = $(e.target);

                // Get the BootstrapValidator instance
                var bv = $form.data('bootstrapValidator');

                var self = this;
                $form.find('.error-container').html("").hide();

                $.post($form.attr('action'), $form.serialize(), function (result) {
                    if (result.code != 1) {
                        $form.find('.error-container').html(result.message).show();
                    } else {
                        window.location.reload();
                    }
                    console.log(result);
                }, 'json');
            });
        });
    </script>
@endsection
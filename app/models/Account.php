<?php

use LaravelBook\Ardent\Ardent;


class Account extends Ardent
{
    protected $table = "accounts";
    protected $fillable = ['name', 'password'];
    protected $hidden = array('password');

    public static $rules = array(
        'name'     => 'required',
        'password' => 'required',
    );


//    public $autoHydrateEntityFromInput = true;    // hydrates on new entries' validation
//    public $forceEntityHydrationFromInput = true; // hydrates whenever validation is called


    /**
     * Get owner of this account
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('User');
    }


}
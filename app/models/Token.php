<?php

class Token extends \Eloquent
{
    const LIFETIME = 600; // 10 min

    protected $fillable = ['user_id', 'token', 'expires_at'];

    public function user()
    {
        return \User::findOrFail($this->user_id);
    }

    public static function boot()
    {
        parent::boot();

        Token::saving(function (Token $token) {
            $exp = \Carbon\Carbon::now();
            $exp->addSeconds(Token::LIFETIME);
            $token->expires_at = $exp;
        });
    }

    public function isTokenValid()
    {
        $now = \Carbon\Carbon::now();
        $exp = \Carbon\Carbon::parse($this->expires_at);

        $diff = $now->diffInSeconds($exp, false);
        if($diff <= 0){
            return false;
        } else {
            return true;
        }
    }


}
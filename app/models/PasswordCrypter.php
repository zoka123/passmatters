<?php

class PasswordCrypter
{

    const IV_APPEND_DELIMITER = "|::|";

    public $algo = MCRYPT_RIJNDAEL_128;
    public $mode = MCRYPT_MODE_CBC;
    public $salt;
    public $keyLength;
    public $iterations = 120000;


    public function __construct()
    {
        $this->salt = strtoupper(sha1("passmatters.eu"));
        $this->keyLength = mcrypt_get_key_size($this->algo, MCRYPT_MODE_CBC);
    }

    /**
     * Setup configuration via array.
     * Available keys are:
     * - algo - algorithm used for encryption
     * - mode - crypt mode
     * - salt
     * - keyLength
     * - iterations
     *
     * @param $array
     */
    public function setup($array)
    {
        foreach ($array as $key => $item) {
            $this->$key = $item;
        }
    }


    /**
     * Returns generated IV for encryption
     * @return string
     */
    public function getIv()
    {
        return mcrypt_create_iv(mcrypt_get_iv_size($this->algo, $this->mode), MCRYPT_RAND);
    }


    /**
     * Returns encryption key generated from provided masterPassword
     * @param $masterPassword
     * @return mixed
     */
    public function getEncryptionKeyFromPassword($masterPassword)
    {
        return hash_pbkdf2('sha1', $masterPassword, $this->salt, $this->iterations, $this->keyLength, true);
    }


    /**
     * Returns encrypted string encoded in base64
     * @param $masterPassword
     * @param $password
     * @return string
     */
    public function encryptPassword($masterPassword, $password)
    {
        $iv = $this->getIv();
        $encryption = base64_encode(mcrypt_encrypt($this->algo, $this->getEncryptionKeyFromPassword($masterPassword), $password, $this->mode, $iv));

        return $this->appendIv($encryption, $iv);
    }


    /**
     * Decryptes password encoded in base 64
     * @param $encrypted_string
     * @param $password
     * @param $iv
     * @return string
     */
    public function decryptPassword($encrypted_string, $password)
    {
        $data = $this->splitPasswordAndIv($encrypted_string);

        return rtrim(mcrypt_decrypt($this->algo, $this->getEncryptionKeyFromPassword($password), base64_decode($data[0]), $this->mode, $data[1]));
    }


    /** Appends IV to the end of encrypted password
     * @param $password
     * @param $iv
     * @return string
     * @throws Exception
     */
    protected function appendIv($password, $iv)
    {
        $iv = base64_encode($iv);
        if (strpos($password, self::IV_APPEND_DELIMITER) !== false || strpos($iv, self::IV_APPEND_DELIMITER) !== false) {
            throw new Exception('Password or IV has delimiter string');
        }

        return $password . self::IV_APPEND_DELIMITER . $iv;
    }


    /**
     * Returns array with password and delimiter
     * array[0] => password
     * array[1] => iv
     * @param $string
     * @return array
     */
    protected function splitPasswordAndIv($string)
    {
        $data = explode(self::IV_APPEND_DELIMITER, $string);
        $data[1] = base64_decode($data[1]);

        return $data;

    }


    /**
     * Re-encrypt password with new master password
     * @param $oldMasterPassword
     * @param $newMasterPassword
     * @param $password
     * @return string
     */
    public function reCryptStringWithNewMaster($oldMasterPassword, $newMasterPassword, $encryptedString)
    {
        $plain = $this->decryptPassword($encryptedString, $oldMasterPassword);

        return $this->encryptPassword($newMasterPassword, $plain);
    }
}

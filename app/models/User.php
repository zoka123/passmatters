<?php

class User extends \Zantolov\Zamb\Models\User
{
    public function accounts()
    {
        return $this->hasMany('Account');
    }

    public function isOwnerOfAccount(Account $account)
    {
        return $account->user_id === $this->id;
    }

    public function tokens()
    {
        return $this->hasMany('Token');
    }


}
<?php


Route::group(array('before' => 'auth'), function () {
    Route::get('/', array('as' => 'Site.Home', 'uses' => 'AppController@index'));
    Route::get('/', array('as' => 'App.Dashboard', 'uses' => 'AppController@index'));
    Route::get('/edit-profile', array('as' => 'App.EditProfile', 'uses' => 'AppController@editProfile'));
});


// Group Admin routes with custom prefix
Route::group(array('prefix' => 'admin'), function () {

    Route::get('/home', array('as' => 'Admin.Dashboard', 'uses' => 'AdminDashboardController@index'));

    Zamb::registerAdminRoutes();
});


// Confide routes
// @todo Use Zamb routes

Route::get('users/create', array('as' => 'user.create', 'uses' => 'UsersController@create'));
Route::post('users', 'UsersController@store');
Route::get('users/login', array('as' => 'user.login', 'uses' => 'UsersController@login'));
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('users/logout', array('as' => 'user.logout', 'uses' => 'UsersController@logout'));


/*
|--------------------------------------------------------------------------
| Api Routes
|--------------------------------------------------------------------------
*/
Zamb::registerApiRoutes();
Route::post('api/login', array('as' => 'Api.login', 'uses' => 'ApiController@login'));
Route::group(array('prefix' => 'api', 'before' => 'api_auth'), function () {

    Route::post('/addAccount', array('as' => 'Api.addAccount', 'uses' => 'ApiController@addAccount'));
    Route::get('/listAccounts', array('as' => 'Api.listAccounts', 'uses' => 'ApiController@listAccounts'));
    Route::get('/getAccount/{id}', array('as' => 'Api.getAccount', 'uses' => 'ApiController@getAccount'));
    Route::post('/updateAccount/{id}', array('as' => 'Api.updateAccount', 'uses' => 'ApiController@updateAccount'));
    Route::post('/deleteAccount/{id}', array('as' => 'Api.deleteAccount', 'uses' => 'ApiController@deleteAccount'));
    Route::post('/logout', array('as' => 'Api.logout', 'uses' => 'ApiController@logout'));
    Route::post('/editProfile', array('as' => 'Api.EditProfile', 'uses' => 'ApiController@editProfile'));

});


Entrust::routeNeedsRole('app*', array('user', 'admin'), Redirect::to(route('user.login')), false);

Zamb::registerAppRoutes();
Zamb::registerAccessRules();

Route::any('testiranje/app', function () {

    $pc = new PasswordCrypter();
    dd($pc->encryptPassword("123456", "password"));

//    dd(mcrypt_get_key_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC));
//    $encrypt = Account::encryptPassword("123456", "zoka");
//    dd($encrypt);

});
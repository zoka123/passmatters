var express = require('express');
var path = require('path');
var logger = require('morgan');
var app = express();
var sqlite3 = require('sqlite3').verbose();
var net = require('net');
var HashMap = require('hashmap').HashMap;
var tls = require('tls');
var fs = require('fs');


var config = {
    ip: 'passmatters.eu',
    tcp_port: '1337',
    ssl_port: '8000'
};

var clients = new HashMap();

app.use(logger('dev'));

/**
 * Set up CA chain for SSL certificate validation
 * @type {Array}
 */
ca = [];
chain = fs.readFileSync("/etc/ssl/passmatters_eu.ca-bundle", 'utf8');
chain = chain.split("\n");
cert = [];

for (_i = 0, _len = chain.length; _i < _len; _i++) {
    line = chain[_i];
    if (!(line.length !== 0)) {
        continue;
    }
    cert.push(line);
    if (line.match(/-END CERTIFICATE-/)) {
        ca.push(cert.join("\n"));
        cert = [];
    }
}

/**
 * SSL server options
 * @type {{ca: (Array|*), key: *, cert: *}}
 */
var sslOptions = {
    ca: ca,
    key: fs.readFileSync('/etc/ssl/passmatters_eu.key.pem'),
    cert: fs.readFileSync('/etc/ssl/passmatters_eu.crt.pem'),
};

/**
 * Authenticate user
 * @param token
 * @param callback
 */
function authenticateUser(token, callback) {
    var db = new sqlite3.Database('../../app/storage/production.sqlite');
    db.serialize(function () {
        db.get("SELECT u.* from users u join tokens t on t.user_id = u.id AND t.token = ? ", token, function (err, row) {
            callback(row);
        });
    });
    db.close();
}


/**
 * Http api endpoint for password request.
 * @param token
 * @param account_id
 */
app.get('/api/requestPassword', function (req, res) {
    var token = req.query.token;
    var account_id = req.query.account_id;

    authenticateUser(token, function (user) {
        if (typeof user === "undefined") {
            res.send({status: 0, message: 'Not authenticated'});
        } else {
            processHttpRequest(user, account_id, 
		function (data, socket) {
			// Success
			socket.write(data.password + '\n');
	                res.send({status: 1, message: 'SENT'});
        	},
		function(data){
			// Error
			res.send({status: 0, message: 'ACCOUNT WITH THAT ID NOT FOUND'});
		}); 
        }
    });
});


/**
 * Test Api endpoint
 */
app.get('/test', function (req, res) {
    res.send({test: "TEST"});
});


/**
 * Returns password for given account id
 * @param id
 * @param user
 * @param callback
 */
function getPasswordByAccountId(id, user, callback) {
    var db = new sqlite3.Database('../../app/storage/production.sqlite');
    db.serialize(function () {
        db.get("SELECT a.password from accounts a where a.id = ? AND a.user_id = ?", id, user.id, function (err, row) {
            callback(row);
        });
    });
    db.close();
}


/**
 * Process http request
 * @param user
 * @param account_id
 * @param callback
 */
function processHttpRequest(user, account_id, successCallback, errorCallback) {
    var socket = clients.get(user.id);

    if (typeof socket === "undefined") {
       errorCallback({status: 0, message: 'CLIENT NOT CONNECTED'});
    } else {
        getPasswordByAccountId(account_id, user, function (data) {
            successCallback(data, socket);
        });
    }
}


/**
 * TCP Server
 */
var messages = {
    general_error: "{status: 0, message: \"Error\"}\n",
    client_connected: "{status: 1, message: \"Connected\"}\n",
    invalid_request: "{status: 0, message: \"Invalid request\"}\n",
    invalid_step: "{status: 0, message: \"Invalid step\"}\n",
    user_not_found: "{status: 0, message: \"User not found\"}\n",
    authentication_success: "{status: 1, message: \"Authenticated\"}\n"
};

var steps = {
    connection: 0,
    authentication: 1,
    requestForPassword: 2,
    terminateConnection: 3
};

function onDataEventListener(data) {
    var socket = this;
    try {
        data = JSON.parse(data);
    } catch (e) {
        socket.write(messages.invalid_request);
        console.log(e);
        return;
    }

    try {
        switch (data.step) {
            case steps.authentication:
                var user = authenticateUser(data.token, function (user) {
                    if (user != null) {
                        socket.write(messages.authentication_success);
                        clients.set(user.id, socket);
                    } else if (typeof user === "undefined") {
                        socket.write(messages.user_not_found);
                    }
                });
                break;
            case steps.terminateConnection:
                socket.end();
                break;
            default:
                socket.write(messages.invalid_step);
                return;
        }
    }
    catch (e) {
        socket.write(messages.general_error);
        console.log(e);
    }
}


// Create TLS Server
var sslServer = tls.createServer(sslOptions, function (cleartextStream) {
    cleartextStream.setEncoding('utf8');
    var nextStep = steps.connection;
    console.log("New client arrived\r\n");
    cleartextStream.write(messages.client_connected);
    cleartextStream.on('data', onDataEventListener);
});

sslServer.listen(config.ssl_port, function () {
    console.log('SSL server bound');
});


// Create TCP Server
var server = net.createServer(function (socket) {
    var nextStep = steps.connection;
    console.log("New client arrived\r\n");
    socket.write(messages.client_connected);
    socket.on('data', onDataEventListener);
});
server.listen(config.tcp_port, config.ip);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;

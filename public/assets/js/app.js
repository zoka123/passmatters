app_controller = {

    init: function () {
        var self = this;
        $form = $("#accountForm");
        self.$form = $form;
        self.events($form);

    },

    startLoading: function () {
        $(".loader").show();
    },

    endLoading: function () {
        $(".loader").hide();
    },

    events: function ($form) {
        var self = this;

        $(".account-trigger").on('click', function () {

            self.startLoading();
            var id = $(this).attr('data-id');
            $.getJSON(self.get_account_url + id, function (json) {
                self.endLoading();
                self.loadForm($form, json)
            });
        });

        $(".deleteTrigger").on('click', function () {
            self.deleteAccount()
        });

    },

    loadForm: function ($form, data) {
        var self = this;
        self.clearForm($form);

        $form.find('*[name="name"]').val(data.name);
        $form.find('*[name="account_id"]').val(data.id);
    },

    clearForm: function ($form) {
        $form.data('bootstrapValidator').resetForm();
        $form.find('*[name="name"]').val("");
        $form.find('*[name="password"]').val("");
        $form.find('*[name="password_confirmation"]').val("");
        $form.find('*[name="master_password"]').val("");
        $form.find('*[name="account_id"]').val(0);
        $form.find('.error-container').html("").hide();


    },

    addNew: function () {
        var self = this;
        self.clearForm(self.$form);
    },

    showError: function ($form, error) {
        $form.find('.error-container').html(error).show();
    },

    submitForm: function ($form) {
        var self = this;

        $form.find('.error-container').html("").hide();

        var account_id = $form.find('*[name="account_id"]').val();
        if (account_id == 0) {
            $.post($form.attr('action'), $form.serialize(), function (result) {
                if (result.code != 1) {
                    self.showError($form, result.message);
                } else {
                    window.location.reload();
                }

                console.log(result);
            }, 'json');
        } else {
            $.post(self.update_account_url + account_id, $form.serialize(), function (result) {
                if (result.code != 1) {
                    self.showError($form, result.message);
                } else {
                    window.location.reload();
                }

                console.log(result);
            }, 'json');
        }
    },

    deleteAccount: function () {
        var self = this;

        var account_id = $form.find('*[name="account_id"]').val();

        if (account_id == 0) {
            self.showError($form, 'Select account for deleting');
            return false;
        }


        if (!confirm('Are you sure?')) {
            return;
        }

        var token = $form.find('*[name="_token"]').val();

        $.post(self.delete_account_url + account_id, {
            master_password: $form.find('*[name="master_password"]').val(),
            _token: token
        }, function (result) {

            if (result.code != 1) {
                self.showError($form, result.message);
            } else {
                window.location.reload();
            }

            console.log(result);
        }, 'json');
    }


}
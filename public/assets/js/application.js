var modal;

var app = {

    CRUDUpdated: function(message) {
        oTable.fnReloadAjax();
        modal.unbind('hide.bs.modal');
        modal.modal("hide");

        if (message) {
            showNotification(message, 'success');
        }
    },

    CRUDDeleted: function(message) {
        oTable.fnReloadAjax();
        modal.modal("hide");

        if (message) {
            showNotification(message, 'success');
        }
    }

}


function initDatatables(element, source_url) {
    var oTable;
    oTable = element.dataTable({
        "oLanguage": {
            "sLengthMenu": "_MENU_ records per page"
        },
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": source_url,
        "fnDrawCallback": function (oSettings) {
            setIframeListener();
        }
    });
    return oTable;
}


function setIframeListener() {
    $('*[data-toggle="iframe-modal"]').on("click", function () {
        var src = $(this).attr("data-iframe-src");
        $("#iframe-modal iframe").attr("src", src);
        modal = $("#iframe-modal").modal();
    });
}

function showNotification(message, css_class) {
    var notif = $("<div>").attr({class: 'alert-sm alert alert-' + css_class}).text(message);
    notif.append($("<button>").attr({class: "close", "data-dismiss": "alert"}).text('x'));
    $('.notifications').html(notif);

    setTimeout(function () {
        $('.notifications').fadeOut(function () {
            $(this).html("");
            $(this).toggle();
        });

    }, 3000);

}

$(function () {
    $(".select2").select2();
})
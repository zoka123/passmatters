<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class AcceptanceHelper extends \Codeception\Module
{
    public static function login($I)
    {
        $I->fillField('#email', 'user');
        $I->fillField('#password', 'user');
        $I->click('button[type="submit"]');
    }

    public static function logout($I)
    {
        $I->click('user', '.dropdown');
        $I->click('Logout', '.dropdown-menu li');
    }

}

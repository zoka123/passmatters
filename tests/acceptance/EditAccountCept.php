<?php

$I = new AcceptanceTester($scenario);
$I->wantTo('Edit account');
$I->amOnPage('/');

\Codeception\Module\AcceptanceHelper::login($I);

$accountName = 'test' . time();

$I->dontSee($accountName);

$I->fillField('#accountForm input[name="name"]', $accountName);
$I->fillField('#accountForm input[name="password"]', 'TestAccountPassword');
$I->fillField('#accountForm input[name="password_confirmation"]', 'TestAccountPassword');
$I->fillField('#accountForm input[name="master_password"]', 'user');
$I->click('#accountForm button[type="submit"].btn-success');

$I->wait(2);
$I->see($accountName);

// EDIT
$I->click($accountName);
$I->fillField('#accountForm input[name="name"]', '');
$I->fillField('#accountForm input[name="name"]', 'EDITED ACCOUTN NAME');
$I->fillField('#accountForm input[name="password"]', 'TestAccountPassword');
$I->fillField('#accountForm input[name="password_confirmation"]', 'TestAccountPassword');
$I->fillField('#accountForm input[name="master_password"]', 'user');
$I->click('#accountForm button[type="submit"].btn-success');

$I->wait(2);
$I->dontSee($accountName);
$I->see('EDITED ACCOUTN NAME');


$I->click('EDITED ACCOUTN NAME');
$I->fillField('#accountForm input[name="master_password"]', 'user');
$I->click('.deleteTrigger');
$I->acceptPopup();
$I->wait(2);
$I->dontSee('EDITED ACCOUTN NAME');
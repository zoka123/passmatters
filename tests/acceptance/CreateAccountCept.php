<?php

$I = new AcceptanceTester($scenario);
$I->wantTo('Create new account');
$I->amOnPage('/');

\Codeception\Module\AcceptanceHelper::login($I);

$accountName = 'test' . time();

$I->dontSee($accountName);

$I->fillField('#accountForm input[name="name"]', $accountName);
$I->fillField('#accountForm input[name="password"]', 'TestAccountPassword');
$I->fillField('#accountForm input[name="password_confirmation"]', 'TestAccountPassword');
$I->fillField('#accountForm input[name="master_password"]', 'user');
$I->click('#accountForm button[type="submit"].btn-success');

$I->wait(2);
$I->see($accountName);

$I->click($accountName);
$I->fillField('#accountForm input[name="master_password"]', 'user');
$I->click('.deleteTrigger');
$I->acceptPopup();
$I->wait(2);
$I->dontSee($accountName);
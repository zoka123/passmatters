<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('Login and logout');


$I->amOnPage('');

//Check if redirects unlogged users
$I->seeInCurrentUrl('users/login');
\Codeception\Module\AcceptanceHelper::login($I);

// Check if user is redirected on home
$I->seeCurrentUrlEquals('/');


\Codeception\Module\AcceptanceHelper::logout($I);
$I->seeInCurrentUrl('users/login');
$I->amOnPage('');
$I->seeInCurrentUrl('users/login');